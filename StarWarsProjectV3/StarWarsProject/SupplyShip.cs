﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StarWarsProject
{
    class SupplyShip:SpaceShip
    {
        public SupplyShip()
            : base(10, 40)
        { 
        }

        public SupplyShip(int w, int p)
        {
            if (p > w)
            {
                eq = new Equipment(w, p);
            }
        }
    }
}
