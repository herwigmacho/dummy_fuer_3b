﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StarWarsProject
{
    public class SpaceShip
    {
        public byte Health { get; private set; }
        public Equipment eq { get; protected set; }

        //Konstruktor(en)
        public SpaceShip() {
            Health = 255;//standard HIER änderbar
            eq = new Equipment();
        }

        public SpaceShip(int weapon, int armour):this
            (new Equipment(weapon,armour))//Konstrukor überladen
        {
            //nothing more to do
        }
        //Konstruktor überladen
        public SpaceShip( Equipment e):this()
        {
            eq = e;
        }

        // Treffer einstecken
        public void Hit(int strength)
        {
            if (eq.Armour < strength)
            {
                if (Health > (strength - eq.Armour))
                    Health -= (byte)(strength - eq.Armour);
                else
                    Health = 0;
            }

            //Panzerung wird "langsam" beschädigt ;)
            eq.DamageArmour();
        }

        //Selbstdarstellung des Schiffes
        public override string ToString()
        {
            return "Leben: " + Health + ", equipped with: " + eq;//ToString()
        }
    }
}
