﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StarWarsProject
{
    public class Fleet
    {
        // Store list of Spaceships
        List<SpaceShip> ls = new List<SpaceShip>();

        //Constructor not needed

        //Add
        public void Add(SpaceShip s)
        {
            ls.Add(s);
        }

        //Remove
        public void Remove(SpaceShip s)
        {
            ls.Remove(s);
        }

        //some getters
        public int Count{
            get { return ls.Count; }
        }

        //Indexer - verwende Flotte wie Array (oder Liste)
        public SpaceShip this[int i]
        {
            get
            {
                try
                {
                    return ls[i];
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        //ToString
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var item in ls)
            {
                sb.Append(item + "\n");    
            }
            return sb.ToString();

        }

    }

    public class BetterFleet : List<SpaceShip>
    {
        // use everything from List

        // just use new ToString()
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var item in this)
            {
                sb.Append(item + "\n");
            }
            return sb.ToString();

        }
    }
}
