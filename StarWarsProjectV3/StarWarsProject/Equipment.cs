﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StarWarsProject
{
    public class Equipment
    {
        // Armour in Langschreibweise
        int armour;
        public int Armour { 
            get { return armour; } 
            private set { armour=value;}
        }

        // Weapon in Kurzschreibweise
        public int Weapon { get; private set; }

        // Konstruktor
        public Equipment()
        {
            Armour = Weapon = 25;
        }

        public Equipment(int weapon, int armour)
        {
            if (weapon + armour <= 50)
            {
                Weapon = weapon;
                this.armour = armour;
            }
        }

        // Div. Methods
        public void DamageArmour()
        {
            if (Armour>0)
                Armour--;
            //sonst bleibts auf 0
        }

        //Selbstdarstellung des Equipment
        public override string ToString()
        {
            return "A=" + armour + ", W=" + Weapon;
        }
    }
}
