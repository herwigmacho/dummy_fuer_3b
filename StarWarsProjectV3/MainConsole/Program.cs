﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using StarWarsProject;

namespace MainConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Ships
            SpaceShip s1 = new SpaceShip(23, 16);
            Console.WriteLine(s1);
            SpaceShip s2 = new SpaceShip( 23, 44);
            Console.WriteLine(s2);
            
            Equipment one = new Equipment(10, 40);
            Equipment two = new Equipment(40, 10);

            SpaceShip s3 = new SpaceShip(one);
            SpaceShip s4 = new SpaceShip(two);
            Console.WriteLine(s3);
            Console.WriteLine(s4);
            #endregion
            s4.Hit(20);
            Console.WriteLine(s4);

            #region fleet1
            Fleet earth = new Fleet();
            earth.Add(s1);
            earth.Add(s2);
            #endregion

            #region fleet2
            Fleet moon = new Fleet();
            moon.Add(s3);
            moon.Add(s4);
            Console.WriteLine("----------\nEarth:");
            Console.WriteLine(earth);
            Console.WriteLine("----------\nMoon:");
            Console.WriteLine(moon);
            #endregion

            #region betterfleet
            Console.WriteLine("----------\nbetter:");
            BetterFleet better = new BetterFleet();
            better.Add(s1);
            better.Add(s2);
            Console.WriteLine(better);
            #endregion
        }
    }
}
