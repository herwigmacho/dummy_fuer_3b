﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StarWarsProject;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethodEquipment1()
        {
            Equipment e = new Equipment(25,25);
            Assert.AreEqual(e.Armour, 25);
            Assert.AreEqual(e.Weapon, 25);
        }
        [TestMethod]
        public void TestMethodEquipment2()
        {
            Equipment e = new Equipment(20, 30);
            Assert.AreEqual(e.Armour, 30);
            Assert.AreEqual(e.Weapon, 20);
            e = new Equipment(30, 30);
            Assert.AreEqual(e.Armour, 0);
            Assert.AreEqual(e.Weapon, 0);
            e = new Equipment(40, 10);
            Assert.AreEqual(e.Armour, 10);
            Assert.AreEqual(e.Weapon, 40);
            e = new Equipment(70, 0);
            Assert.AreEqual(e.Armour, 0);
            Assert.AreEqual(e.Weapon, 0);
            e = new Equipment();
            Assert.AreEqual(e.Armour, 25);
            Assert.AreEqual(e.Weapon, 25);
        }
      
        [TestMethod]
        public void TestMethodSpaceship1()
        {
            SpaceShip s = new SpaceShip();
            Assert.IsNotNull(s.eq);
            //Equipment OBEN getestet
            s.Hit(20); // durch Treffer wird A
            Assert.AreEqual(s.Health, 255);
            Assert.AreEqual(s.eq.Armour, 24);   //Armour weniger
            s.Hit(50);                          //Treffer
            Assert.AreEqual(s.Health, 229);     //um 26 reduziert da schon 1x getroffen!!

            s = new SpaceShip(0, 0);
            s.Hit(20);
            Assert.AreEqual(s.Health, 235);
            s.Hit(234);
            Assert.AreEqual(s.Health, 1);
            s.Hit(2);
            Assert.AreEqual(s.Health, 0);

        }
        [TestMethod]
        public void TestMethodFleet1() {

            Fleet f = new Fleet();
            Assert.AreEqual(f.Count, 0);

            SpaceShip s = new SpaceShip();
            
            f.Add(s);
            Assert.AreEqual(f.Count, 1);
            Assert.AreEqual(f[0], s);       //Verwendung des Indexer f[0]


        }
        [TestMethod]
        public void TestMethodBetterFleet1()
        {

            BetterFleet f = new BetterFleet();
            Assert.AreEqual(f.Count, 0);

            SpaceShip s = new SpaceShip();

            f.Add(s);
            Assert.AreEqual(f.Count, 1);
            Assert.AreEqual(f[0], s);       //Verwendung des Indexer f[0]

        }
    }
}
