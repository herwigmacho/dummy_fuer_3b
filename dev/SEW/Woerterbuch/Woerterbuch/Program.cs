﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Woerterbuch
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string,string> myDict= new Dictionary<string,string>();
            myDict.Add("Haus", "house");
            myDict.Add("Maus", "mouse");

            foreach (var item in myDict)
            {
                Console.WriteLine(item.Key + "-" + item.Value);
            }
            Console.WriteLine("--------------------");
            Console.WriteLine(myDict["Maus"]);
        }
    }
}
